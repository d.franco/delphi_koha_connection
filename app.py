from flask import Flask, jsonify, request
from operationBD import koha
import json


app = Flask(__name__)


@app.route ('/')
def index():            
    return '<h1>Api Rest de delphi para insercion y modificacion</h1>'


@app.route ('/getAll')
def getAll():            
    kohas =koha()
    res =kohas.getAll()
    return jsonify(res)


@app.route ('/get', methods = ['POST'])
def get():            
    kohas =koha()
    res = kohas.getJson(request.json)
    json_in_str = json.dumps(res, sort_keys=True, default=str)
    return jsonify(json.loads(json_in_str))


@app.route ('/insert', methods = ['POST'])
def insert():            
    kohas =koha()
    res = kohas.insertJson(request.json )
    return jsonify(res)


@app.route ('/update', methods = ['POST'])
def update():            
    kohas =koha()
    res =kohas.actualizar(request.json)
    return jsonify(res)


if __name__ == '__main__':
    app.run(debug=False, port=8083)
