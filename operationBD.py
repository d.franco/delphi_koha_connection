from datetime import date
from datetime import datetime
from funtions_utils.create_xml import create_xml, create_marc
from funtions_utils.create_conexion import run_query
from funtions_utils.json_from_bd_koha import create_json_koha_bd


class koha():   
    def insertJson(self, data ):
        query = "SELECT max(biblionumber) FROM biblio" 
        result = run_query(query) 
        resultado = result[0][0] + 1
        biblio = data['biblio']
        biblioItems = data['biblioItems']
        items = data['items']
        campo = data['campos'] 
        indicadores = data['indicadores']
        biblio['biblionumber'] = resultado
        biblionumber = str(resultado) # aqui debbe ir el biblionumber
        xml = create_xml(campo,biblionumber, indicadores)
        marc = create_marc(xml)

        query1 = """INSERT INTO `biblio` ( `frameworkcode`, `author`, `title`, `unititle`, `notes`, 
        `seriestitle`, `datecreated`, `abstract`)
        VALUES ( '%s', '%s', '%s', '%s', '%s', '%s', '%s',NULL);
        """ % (biblio['frameworkcode'],biblio['author'],biblio['title'], biblio['unititle'], biblio['notes'],biblio['seriestitle'], biblio['datecreated'])


        query2 = """INSERT INTO `biblioitems` ( `biblioitemnumber`,`biblionumber`, `volume`, `number`, `itemtype`, 
        `isbn`, `issn`, `ean`, `publicationyear`, `publishercode`, `volumedate`, `volumedesc`, `collectiontitle`, `collectionissn`, `collectionvolume`, `editionstatement`, `editionresponsibility`, 
        `timestamp`,`illus`, `pages`, `notes`, `size`, `place`, `lccn`, `url`, `cn_source`, `cn_class`, `cn_item`, `cn_suffix`, `cn_sort`, `agerestriction`, `totalissues`,`marcxml`, `marc`) 
        VALUES (%i, %i,NULL,NULL,'%s',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
        CURRENT_TIMESTAMP,'%s','%s',NULL,'%s',NULL,NULL,'%s','%s', NULL, NULL, NULL ,'%s', NULL,NULL,'%s', '%s');
        """ % (resultado,resultado,biblioItems['itemtype'],
        biblioItems['illus'],biblioItems['pages'],biblioItems['size'],biblioItems['url'],biblioItems['cn_source'],biblioItems['cn_sort'], xml, marc)


        query3 = """ INSERT INTO `items` (`biblionumber`, `biblioitemnumber`, `barcode`, `dateaccessioned`, `booksellerid`, `homebranch`, `price`, `replacementprice`, `replacementpricedate`, 
        `datelastborrowed`, `datelastseen`, `stack`, `notforloan`, `damaged`, `itemlost`, `itemlost_on`, `withdrawn`, `withdrawn_on`, `itemcallnumber`, 
        `coded_location_qualifier`, `issues`, `renewals`, `reserves`, `restricted`, `itemnotes`, `itemnotes_nonpublic`, `holdingbranch`, `paidfor`, `timestamp`, 
        `location`, `permanent_location`, `onloan`, `cn_source`, `cn_sort`, `ccode`, `materials`, `uri`, `itype`, `more_subfields_xml`, 
        `enumchron`, `copynumber`, `stocknumber`, `new_status`) 
        VALUES ( %i, %i, NULL, '%s', NULL, NULL, NULL, NULL, NULL,
        NULL, '%s', NULL, %i, %i, %i, NULL, %i , NULL,'%s',
        NULL,NULL , NULL, NULL, NULL, NULL, NULL, NULL ,NULL,CURRENT_TIMESTAMP,
        '%s', '%s', NULL, '%s', '%s', NULL, NULL, NULL,'%s',NULL,
        NULL,NULL,NULL,NULL);
        """ % (resultado, resultado, items['dateaccessioned'], 
        items['datelastseen'], items['notforloan'], items['damaged'], items['itemlost'], items['withdrawn'], items['itemcallnumber'],
        items['location'], items['permanent_location'], items['cn_source'] , items['cn_sort'], items['itype']  
        )


        queryInfo ="SELECT `biblionumber` FROM `biblio` WHERE 1"
        consulta = run_query(queryInfo)
        numbers =list(consulta)
        l= len(numbers) -1
        esta= True
        while l >= 0:
            if biblio['biblionumber'] in list(numbers[l]):
                esta = False
            l= l-1
        if esta:
            run_query(query1)
            run_query(query2)
            run_query(query3)
            proces = {"se agrego la ficha":resultado}
            return proces
        else:
            res = {"error":"el id de la ficha ya existe","codigo":403,"numero id de la ficha a insertar": biblio['biblionumber']}
            return res

        
    def getAll(self):
        queryInfo ="SELECT * FROM items;"
        consulta = run_query(queryInfo)
        return {"respuesta":consulta}


    def getJson(self, miniJson):
        num= miniJson['numero']
        queryInfo ="SELECT `biblionumber` FROM `biblio` WHERE 1"
        consulta = run_query(queryInfo)
        numbers =list(consulta)
        l= len(numbers) -1
        esta= False
        while l >= 0:
            if num in list(numbers[l]):
                esta = True
            l= l-1
        if esta:
            queryA = "SELECT * FROM `biblio` WHERE `biblionumber` = %i;"% num
            queryB = "SELECT * FROM `biblioitems` WHERE `biblionumber` = %i;"% num
            queryC = "SELECT * FROM `items` WHERE `biblionumber` = %i;"% num
            resultA = run_query(queryA)
            resultB = run_query(queryB)
            resultC = run_query(queryC)
            devJson = create_json_koha_bd(resultA, resultB, resultC)
            return devJson
        else:
            res = {"error":"el numero del id de la ficha buscada no esta","codigo":403,"numero Buscado ":num}
            return res


    def actualizar(self,datosActualizar):
        for key, value in datosActualizar.items():
            if key == "biblionumber": 
                biblionumber = value
        try:
            run_query('SELECT `marcxml` FROM `biblioitems` WHERE `biblioitems`.`biblionumber` = '+biblionumber)
        except:
            return {"error":"debe diligenciar el parametro de biblionumber"}
        campo = datosActualizar['campos']
        indicadores = datosActualizar['indicadores']
        xml = create_xml(campo, biblionumber, indicadores)
        queryEnviar = f"UPDATE `biblioitems` SET `marcxml` = '{xml}' WHERE `biblioitems`.`biblionumber` = {biblionumber} ; "
        run_query(queryEnviar)
        query3 = f""" UPDATE `items` set itype = "DRP" , itemcallnumber = "{campo["numeroClasificacion"]}" WHERE `biblionumber` = {biblionumber}"""
        run_query(query3)
        return {"ficha actualizada": biblionumber}
