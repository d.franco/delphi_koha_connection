def duplicate_field(fields_received, format_initial, format_final):
        if fields_received == "":
            return format_initial + format_final
        fields = fields_received.split("-|-")
        field_duplicates = ""
        for field in fields:
            field_duplicates += format_initial + field + format_final
        return field_duplicates
    

def get100e(terminoIndicativoFuncion): 
    initial = """<subfield code="e">"""
    final = """</subfield>\n"""
    return duplicate_field(terminoIndicativoFuncion, initial, final)
    

def get490(coleccion, ind1, ind2):
    initial = """<datafield tag="490" """ + f"""ind1="{ind1}" ind2="{ind2}">\n""" +"""    <subfield code="a">"""
    final = """</subfield>\n  </datafield>\n"""
    return duplicate_field(coleccion, initial, final)

def get653(palabraClave, ind1, ind2):
    initial = """<datafield tag="653" """ + f"""ind1="{ind1}" ind2="{ind2}">\n""" +"""    <subfield code="a">"""
    final = "</subfield>\n  </datafield>\n  "
    return duplicate_field(palabraClave, initial, final)



def get650(ind1, ind2,tema, subdivisionGeneral, fuenteEncabezamiento650):
    format_total = """ <datafield tag="650" """ + f"""ind1="{ind1}" ind2="{ind2}">\n""" +"""     <subfield code="a">
    """ + tema + """</subfield>\n    <subfield code="x">""" + subdivisionGeneral + """</subfield>\n     <subfield code="2">""" + fuenteEncabezamiento650 + """</subfield>\n  </datafield>\n
    """
    if tema == "":
        return format_total
    fields_tema = tema.split("-|-")
    fields_subdivision = subdivisionGeneral.split("-|-")
    fields_fuente = fuenteEncabezamiento650.split("-|-")
    cual = 0
    field_duplicates = ""
    for field in fields_tema:
        format_total = """ <datafield tag="650" """ + f"""ind1="{ind1}" ind2="{ind2}">\n"""  +  """<subfield code="2">""" + fields_fuente[cual] + """</subfield>\n""" + """  <subfield code="a">""" + fields_tema[cual] + """</subfield>\n    <subfield code="x"></subfield>\n       </datafield>\n"""
        field_duplicates += format_total
        cual = cual + 1
    return field_duplicates



def get600(ind1, ind2,fuenteEncabezamiento, nombrePersonaTema, fechasAsociadasNombreTema, funcionTema, periodoMandato):
    format_total = """ <datafield tag="600" """ + f"""ind1="{ind1}" ind2="{ind2}">\n""" +"""     <subfield code="2">""" + fuenteEncabezamiento + """</subfield>\n      <subfield code="a">""" + nombrePersonaTema + """</subfield>\n    <subfield code="d">""" + fechasAsociadasNombreTema + """</subfield>\n    <subfield code="e">""" + funcionTema + """</subfield>\n  <subfield code="y">""" + periodoMandato + """</subfield>\n   </datafield>\n"""
    if fuenteEncabezamiento == "":
        return format_total
    fields_Encabezamiento = fuenteEncabezamiento.split("-|-")
    fields_nombrePersona = nombrePersonaTema.split("-|-")
    fields_Asociacion = fechasAsociadasNombreTema.split("-|-")
    fields_funcionTema = funcionTema.split("-|-")
    fields_periodoMandato = periodoMandato.split("-|-")
    cual = 0
    field_duplicates = ""
    for _ in fields_Encabezamiento:
        format_total = """ <datafield tag="600" """ + f"""ind1="{ind1}" ind2="{ind2}">\n""" +"""     <subfield code="2">""" + fields_Encabezamiento[cual] + """</subfield>\n      <subfield code="a">""" + fields_nombrePersona[cual] + """</subfield>\n    <subfield code="d">""" + fields_Asociacion[cual] + """</subfield>\n    <subfield code="e">""" + fields_funcionTema[cual] + """</subfield>\n  <subfield code="y">""" + fields_periodoMandato[cual] + """</subfield>\n   </datafield>\n"""
        field_duplicates += format_total
        cual = cual + 1
    return field_duplicates
