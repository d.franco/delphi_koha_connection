def get_indicadores(indicadores):
    result = {
        "i100_1" : indicadores["campos100indicador1"],
        "i100_2" : indicadores["campos100indicador2"],
        "i110_1" : indicadores["campos110indicador1"],
        "i110_2" : indicadores["campos110indicador2"],
        "i111_1" : indicadores["campos111indicador1"],
        "i111_2" : indicadores["campos111indicador2"],
        "i243_1" : indicadores["campos243indicador1"],
        "i243_2" : indicadores["campos243indicador2"],
        "i264_1" : indicadores["campos264indicador1"],
        "i264_2" : indicadores["campos264indicador2"],
        "i490_1" : indicadores["campos490indicador1"],
        "i490_2" : indicadores["campos490indicador2"],
        "i511_1" : indicadores["campos511indicador1"],
        "i511_2" : indicadores["campos511indicador2"],
        "i600_1" : indicadores["campos600indicador1"],
        "i600_2" : indicadores["campos600indicador2"],
        "i610_1" : indicadores["campos610indicador1"],
        "i610_2" : indicadores["campos610indicador2"],
        "i611_1" : indicadores["campos611indicador1"],
        "i611_2" : indicadores["campos611indicador2"],
        "i650_1" : indicadores["campos650indicador1"],
        "i650_2" : indicadores["campos650indicador2"],      
        "i653_1" : indicadores["campos653indicador1"],
        "i653_2" : indicadores["campos653indicador2"],
        "i655_1" : indicadores["campos655indicador1"],
        "i655_2" : indicadores["campos655indicador2"],
        "i810_1" : indicadores["campos810indicador1"],
        "i810_2" : indicadores["campos810indicador2"]
    }
    return result