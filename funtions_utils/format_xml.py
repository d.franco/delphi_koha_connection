

def get_format_initial_from_xml(encabezamiento, biblionumber):
    try:
        result_xml = """<?xml version="1.0" encoding="UTF-8"?>\n<record\n    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n    xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"\n    xmlns="http://www.loc.gov/MARC21/slim">\n\n  
            <leader>""" + encabezamiento + """</leader>\n  
            <datafield tag="999" ind1=" " ind2=" ">\n
            <subfield code="c">""" + biblionumber+ """</subfield>\n
            <subfield code="d">""" + biblionumber+ """</subfield>\n  </datafield>\n"""
    except:
        result_xml = """<?xml version="1.0" encoding="UTF-8"?>\n<record\n    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n    xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"\n    xmlns="http://www.loc.gov/MARC21/slim">\n\n  
            <leader></leader>\n  
            <datafield tag="999" ind1=" " ind2=" ">\n
            <subfield code="c"></subfield>\n
            <subfield code="d"></subfield>\n  </datafield>\n"""
    return result_xml