from funtions_utils.funtion_duplicated import *
from funtions_utils.get_indicators import get_indicadores
from funtions_utils.format_xml import get_format_initial_from_xml

def create_xml(campo, biblionumber, indicadores):
    
    ind = get_indicadores(indicadores)
    
    encabezamiento = campo["encabezamiento"]
    horaCampoCinco = campo[
        "horaCampoCinco"
    ]  # agregar al formularioo, pero es dato quemado
    caracteristicaseis = campo["caracteristicaseis"]
    descripcionFisicaFijo = campo["descripcionFisicaFijo"]
    longitudFija = campo["longitudFija"]  # 008
    centroCatalogador = campo["centroCatalogador"]  # 040 -a
    lenguaCatalogacion = campo["lenguaCatalogacion"]  # 040 -b
    codigoLenguaTexto = campo["codigoLenguaTexto"]  # 041 -A
    numeroClasificacion = campo["numeroClasificacion"]  # 084
    extension = campo["extension"]  # 300 -a
    caracteristicasFisicas = campo["caracteristicasFisicas"]  # 300 -b
    dimensiones = campo["dimensiones"]  # 300 -c
    materialAcompanante = campo["materialAcompanante"]  # 300 -e
    duracion = campo["duracion"]  # 306 -a
    terminoTipoContenido = campo["terminoTipoContenido"]  # 336 -a
    codigoTipoContenido = campo["codigoTipoContenido"]  # 336 -b
    fuenteTipoContenido = campo["fuenteTipoContenido"]  # 336 -2
    nombreTipoMedio = campo["nombreTipoMedio"]  # 337 -a
    codigoTipoMedio = campo["codigoTipoMedio"]  # 337 -b
    fuenteTipoMedio = campo["fuenteTipoMedio"]  # 337 -2
    nombreTipoSoporte = campo["nombreTipoSoporte"]  # 338 -a
    codigoTipoSoporte = campo["codigoTipoSoporte"]  # 338 -b
    fuenteTipoSoporte = campo["fuenteTipoSoporte"]  # 338 -2
    restriccionesLegales = campo["restriccionesLegales"]  # 506 -a
    terminologiaNormalizadaRestricciones = campo[
        "terminologiaNormalizadaRestricciones"
    ]  # 506 -f
    terminologiaNormalizada = campo["terminologiaNormalizada"]  # 583 -a
    fechaAccion = campo["fechaAccion"]  # 583 -c
    mediosRealizaAccion = campo["mediosRealizaAccion"]  # 583 -i
    codigoCatalogador = campo["codigoCatalogador"]  # 583 -k
    nombreEntidad810 = campo["nombreEntidad810"]  # 810
    localizacion = campo["localizacion"]  # 852 -a
    parteClasificacion = campo["parteClasificacion"]  # 852 -h
    prefijoSignatura = campo["prefijoSignatura"]  # 852 -k
    designacionUnidadFisica = campo["designacionUnidadFisica"]  # 852 -p
    codec = campo["codec"]  # 856 -q
    host = campo["host"]  # 856 -a
    url = campo["url"]  # 856 -u
    textoEnlace = campo["textoEnlace"]  # 856 -y
    fuenteSistemaClasificacion = campo["fuenteSistemaClasificacion"]  # 942 -2
    tipoItemKoha = campo["tipoItemKoha"]  # 942 -c
    autorizacion = campo["autorizacion"]  # 942 -n
    tipoAoV = campo["tipoAoV"]  # v

    numeroClasificacionOpcional = campo["numeroClasificacionOpcional"]  # 090
    nombrePersona = campo["nombrePersona"]  # 100 -a
    fechasAsociadasNombre = campo["fechasAsociadasNombre"]  # 100 -d
    terminoIndicativoFuncion = campo["terminoIndicativoFuncion"]  # 100 -e
    titulosTerminosAsociados = campo["titulosTerminosAsociados"]  # 100 -c
    nombreEntidad = campo["nombreEntidad"]  # 110 -a
    funcion = campo["funcion"]  # 110 -e
    unidadSubordinada = campo["unidadSubordinada"]  # 110 -b
    numeroCongreso = campo["numeroCongreso"]  # 111 -n
    nombreReunion = campo["nombreReunion"]  # 111 -a
    fechaReunion = campo["fechaReunion"]  # 111 -d
    lugarReunion = campo["lugarReunion"]  # 111 -c
    tituloUniforme = campo["tituloUniforme"]  # 243 -a
    fechaTituloUniforme = campo["fechaTituloUniforme"]  # 243 -f
    title = campo["title"]  # 245 -a
    fechaTitulo = campo["fechaTitulo"]  # 245 -f
    mencionResponsabilidad = campo["mencionResponsabilidad"]  # 245 -c
    restoTitulo = campo["restoTitulo"]  # 245 -b
    lugarProduccion = campo["lugarProduccion"]  # 264 -a
    entidadProductora = campo["entidadProductora"]  # 264 -b
    anoProduccion = campo["anoProduccion"]  # 264 -c
    coleccion = campo["coleccion"]  # 490
    notaGeneral = campo["notaGeneral"]  # 500 -a
    especificacionMateriales = campo["especificacionMateriales"]  # 500 -3
    notaContenido = campo["notaContenido"].replace("'", "")  # 505
    notaCreditos = campo["notaCreditos"]  # 508
    notaElenco = campo["notaElenco"]  # 511
    fuenteEncabezamiento = campo["fuenteEncabezamiento"]  # 600 -2
    nombrePersonaTema = campo["nombrePersonaTema"]  # 600 -a
    fechasAsociadasNombreTema = campo["fechasAsociadasNombreTema"]  # 600 -d
    funcionTema = campo["funcionTema"]  # 600 -e
    periodoMandato = campo["periodoMandato"]  # 600 -y
    nombreEntidadTema = campo["nombreEntidadTema"]  # 610
    fuenteEncabezamiento611 = campo["fuenteEncabezamiento611"]  # 611 -2
    numeroCongreso611 = campo["numeroCongreso611"]  # 611 -n
    nombreEventoTema = campo["nombreEventoTema"]  # 611 -a
    fechaCongreso = campo["fechaCongreso"]  # 611 -d
    sedeCongreso = campo["sedeCongreso"]  # 611 -c
    fuenteEncabezamiento650 = campo["fuenteEncabezamiento650"]  # 650 -2
    tema = campo["tema"]  # 650 -a
    subdivisionGeneral = campo["subdivisionGeneral"]  # 650 -x
    palabraClave = campo["palabraClave"]  # 653
    fuenteTermino = campo["fuenteTermino"]  # 655-2
    genero = campo["genero"]  # 655 -a
    forma = campo["forma"]  # 655 -v
       
   
    ind["i655_1"]
    ind["i655_2"]
    ind["i810_1"]
    ind["i810_2"]

    def los100():
        if nombrePersona == "" and fechasAsociadasNombre == "" :
            return ""
        return """<datafield tag="100" """ + f"""ind1="{ind["i100_1"]}" ind2="{ind["i100_2"]}">\n""" +"""    <subfield code="a">""" + nombrePersona + """</subfield>\n    <subfield code="d">""" + fechasAsociadasNombre+ """</subfield>\n    """ + get100e(terminoIndicativoFuncion) + """     <subfield code="e">""" + titulosTerminosAsociados + """</subfield>\n  </datafield>\n  """
    
    def los110():
        if nombreEntidad == "" and unidadSubordinada == "":
            return ""
        return """<datafield tag="110" """ + f"""ind1="{ind["i110_1"]}" ind2="{ind["i110_2"]}">\n""" +"""    <subfield code="a">""" + nombreEntidad + """</subfield>\n    <subfield code="e">""" + funcion + """</subfield>\n    <subfield code="b">""" + unidadSubordinada + """</subfield>\n  </datafield>\n  """


    def los111():
        if numeroCongreso == "" and nombreReunion == "":
            return ""
        return """<datafield tag="111" """ + f"""ind1="{ind["i111_1"]}" ind2="{ind["i111_2"]}">\n""" +"""   <subfield code="n">""" + numeroCongreso + """</subfield>\n    <subfield code="a">""" + nombreReunion + """</subfield>\n    <subfield code="d">""" + fechaReunion + """</subfield>\n    <subfield code="c">""" + lugarReunion + """</subfield>\n  </datafield>\n  """
    
    try:
        xml = (
            get_format_initial_from_xml(encabezamiento, biblionumber) +""" 
        <controlfield tag="001"></controlfield>\n  
        <controlfield tag="003">CO-BoRTV</controlfield>\n  
        <controlfield tag="005">""" + horaCampoCinco + """</controlfield>\n
        <controlfield tag="006">""" + caracteristicaseis + """</controlfield>\n
        <controlfield tag="007">""" + descripcionFisicaFijo + """</controlfield>\n  
        <controlfield tag="008">""" + longitudFija + """</controlfield>\n  
        <datafield tag="010" ind1=" " ind2=" ">\n    <subfield code="a">CO-BoRTV</subfield>\n  </datafield>\n  
        <datafield tag="040" ind1=" " ind2=" ">\n    <subfield code="a">""" + centroCatalogador + """</subfield>\n    <subfield code="b">""" + lenguaCatalogacion + """</subfield>\n  </datafield>\n 
        <datafield tag="041" ind1=" " ind2=" ">\n    <subfield code="a">""" + codigoLenguaTexto + """</subfield>\n  </datafield>\n 
        <datafield tag="084" ind1=" " ind2=" ">\n    <subfield code="a">""" + numeroClasificacion + """</subfield>\n  </datafield>\n  
        <datafield tag="090" ind1=" " ind2=" ">\n    <subfield code="a">""" + numeroClasificacionOpcional + """</subfield>\n  </datafield>\n  
        """+los100()+"""
        """+los110()+"""
        """+los111()+"""
        <datafield tag="243" """ + f"""ind1="{ind["i243_1"]}" ind2="{ind["i243_2"]}">\n""" +"""   <subfield code="a">""" + tituloUniforme + """</subfield>\n     <subfield code="f">""" + fechaTituloUniforme + """</subfield>\n  </datafield>\n  
        <datafield tag="245" ind1="1" ind2="0">\n    <subfield code="a">""" + title + """</subfield>\n    <subfield code="c">""" + mencionResponsabilidad + """</subfield>\n    <subfield code="f">""" + fechaTitulo + """</subfield>\n      <subfield code="b">""" + restoTitulo + """</subfield>\n  </datafield>\n  
        <datafield tag="264" """ + f"""ind1="{ind["i264_1"]}" ind2="{ind["i264_2"]}">\n""" +"""    <subfield code="a">""" + lugarProduccion + """</subfield>\n    <subfield code="b">""" + entidadProductora + """</subfield>\n    <subfield code="c">""" + anoProduccion + """</subfield>\n  </datafield>\n  
        <datafield tag="300" ind1=" " ind2=" ">\n    <subfield code="a">""" + extension + """</subfield>\n    <subfield code="b">""" + caracteristicasFisicas + """</subfield>\n    <subfield code="c">""" + dimensiones + """</subfield>\n   <subfield code="e">""" + materialAcompanante + """</subfield>\n      </datafield>\n  
        <datafield tag="306" ind1=" " ind2=" ">\n    <subfield code="a">""" + duracion + """</subfield>\n  </datafield>\n 
        <datafield tag="336" ind1=" " ind2=" ">\n    <subfield code="a">""" + terminoTipoContenido + """</subfield>\n    <subfield code="b">""" + codigoTipoContenido + """</subfield>\n    <subfield code="2">""" + fuenteTipoContenido + """</subfield>\n  </datafield>\n 
        <datafield tag="337" ind1=" " ind2=" ">\n    <subfield code="a">""" + nombreTipoMedio + """</subfield>\n    <subfield code="b">""" + codigoTipoMedio + """</subfield>\n  <subfield code="2">""" + fuenteTipoMedio + """</subfield>\n  </datafield>\n  
        <datafield tag="338" ind1=" " ind2=" ">\n    <subfield code="a">""" + nombreTipoSoporte + """</subfield>\n    <subfield code="b">""" + codigoTipoSoporte + """</subfield>\n    <subfield code="2">""" + fuenteTipoSoporte + """</subfield>\n  </datafield>\n  
        """ + get490(coleccion, ind["i490_1"], ind["i490_2"]) + """   
        <datafield tag="500" ind1=" " ind2=" ">\n    <subfield code="a">""" + notaGeneral + """</subfield>\n        <subfield code="3">""" + especificacionMateriales + """</subfield>\n  </datafield>\n
        <datafield tag="505" ind1="0" ind2=" ">\n    <subfield code="a">""" + notaContenido + """</subfield>\n  </datafield>\n  
        <datafield tag="506" ind1="0" ind2=" ">\n    <subfield code="a">""" + restriccionesLegales + """</subfield>\n    <subfield code="f">""" + terminologiaNormalizadaRestricciones + """</subfield>\n  </datafield>\n  
        <datafield tag="508" ind1=" " ind2=" ">\n    <subfield code="a">""" + notaCreditos + """</subfield>\n  </datafield>\n  
        <datafield tag="511" """ + f"""ind1="{ind["i511_1"]}" ind2="{ind["i511_2"]}">\n""" +"""     <subfield code="a">""" + notaElenco + """</subfield>\n  </datafield>\n  
        <datafield tag="583" ind1=" " ind2=" ">\n    <subfield code="a">""" + terminologiaNormalizada + """</subfield>\n    <subfield code="c">""" + fechaAccion + """</subfield>\n     <subfield code="i">""" + mediosRealizaAccion + """</subfield>\n     <subfield code="k">delphi_user</subfield>\n  </datafield>\n 
        
        """ + get600({ind["i600_1"]} , ind["i600_2"] , fuenteEncabezamiento, nombrePersonaTema, fechasAsociadasNombreTema, funcionTema, periodoMandato) + """        
        <datafield tag="610" """ + f"""ind1="{ind["i610_1"]}" ind2="{ind["i610_2"]}">\n""" +"""     <subfield code="a">""" + nombreEntidadTema + """</subfield>\n   <subfield code="2"></subfield>\n   <subfield code="l"></subfield>\n    <subfield code="k"></subfield>\n  </datafield>\n  
        <datafield tag="611" """ + f"""ind1="{ind["i611_1"]}" ind2="{ind["i611_2"]}">\n""" +"""     <subfield code="2">""" + fuenteEncabezamiento611 + 
                            """</subfield>\n    <subfield code="n">""" + numeroCongreso611 + """</subfield>\n    <subfield code="a">""" + nombreEventoTema + """</subfield>\n   <subfield code="d">""" + fechaCongreso + """</subfield>\n   <subfield code="c">""" + sedeCongreso + """</subfield>\n   </datafield>\n  
        """ + get650({ind["i650_1"]} , ind["i650_2"] , tema, subdivisionGeneral, fuenteEncabezamiento650) + get653(palabraClave, ind["i653_1"], ind["i653_2"]) +"""
        <datafield tag="655" """ + f"""ind1="{ind["i655_1"]}" ind2="{ind["i655_2"]}">\n""" +"""   <subfield code="2">""" + fuenteTermino + """</subfield>\n    <subfield code="a">""" + genero + """</subfield>\n    <subfield code="v">""" + forma + """</subfield>\n  </datafield>\n  
        <datafield tag="810" """ + f"""ind1="{ind["i810_1"]}" ind2="{ind["i810_2"]}">\n""" +"""   <subfield code="a">"""
            + nombreEntidad810 + """</subfield>\n  </datafield>\n
        <datafield tag="852" ind1=" " ind2=" ">\n    <subfield code="a">"""
            + localizacion + """</subfield>\n    <subfield code="h">"""
            + parteClasificacion + """</subfield>\n     <subfield code="k">"""
            + prefijoSignatura + """</subfield>\n      <subfield code="p">"""
            + designacionUnidadFisica + """</subfield>\n  </datafield>\n
        <datafield tag="942" ind1=" " ind2=" ">\n    <subfield code="2">"""
            + fuenteSistemaClasificacion + """</subfield>\n    <subfield code="c">""" + tipoItemKoha + """</subfield>\n     <subfield code="n">""" + autorizacion + """</subfield>\n  </datafield>\n</record>
        """
        )
    except:
                xml = (
            get_format_initial_from_xml(encabezamiento, biblionumber) +""" 
        <controlfield tag="001"></controlfield>\n  
        <controlfield tag="003">CO-BoRTV</controlfield>\n  """
        )

    return xml



def create_marc(xml_marc):
    inicio = '215315901234586124353, "marcxml": "'
    return inicio + xml_marc +'"'